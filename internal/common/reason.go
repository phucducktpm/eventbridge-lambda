package common

import (
	"errors"
)

type ResponseCode string

const (
	Successfully          ResponseCode = "00"
	ErrSignatureInvalid   ResponseCode = "01"
	ErrAccountNoInvalid   ResponseCode = "02"
	ErrTransactionExpired ResponseCode = "03"
	ErrAccountInactive    ResponseCode = "04"

	BadRequestJson           ResponseCode = "06"
	ErrSignatureRequired     ResponseCode = "07"
	ErrAccountNoRequired     ResponseCode = "08"
	ErrMerchantRequired      ResponseCode = "09"
	ErrOrderIDRequired       ResponseCode = "10"
	ErrAmountRequired        ResponseCode = "11"
	ErrBillNumberRequired    ResponseCode = "12"
	ErrTerminalIDRequired    ResponseCode = "13"
	ErrTransactionIDRequired ResponseCode = "14"
	ErrStatusRequired        ResponseCode = "15"
	ErrStatusInvalid         ResponseCode = "16"
	ErrStatusNeedCreated     ResponseCode = "17"
	ErrAmountMin             ResponseCode = "18"
	ErrAmountMax             ResponseCode = "19"
	ErrLenTranID             ResponseCode = "20"
	ErrLenAccNo              ResponseCode = "21"
	ErrTimeInvalid           ResponseCode = "22"
	ErrLenDescription        ResponseCode = "23"
	ErrOrderIdExists         ResponseCode = "24"
	ErrOrderIdInvalid        ResponseCode = "25"
	ErrUpdateStatusOfUPH     ResponseCode = "26"
	ErrTransProcessed        ResponseCode = "27"
	ErrAccountTypeInvalid    ResponseCode = "28"
	ErrUpdateOKPushErr       ResponseCode = "29"
	ErrAmountInsufficient    ResponseCode = "30"
	ErrEsbAccountStatus      ResponseCode = "31"
	ErrBuildQr               ResponseCode = "32"

	ErrEpassTransNumber     ResponseCode = "33"
	ErrEpassAmount          ResponseCode = "34"
	ErrPartnerInvalid       ResponseCode = "35"
	ErrEpassNotFound        ResponseCode = "36"
	ErrEpassPlateTypeInvaid ResponseCode = "37"
	ErrEpassTopupExists     ResponseCode = "38"

	ErrEpCompareSCode  ResponseCode = "40"
	ErrEpCompareReCode ResponseCode = "41"

	ErrExternalServiceTimeOut ResponseCode = "95"
	ErrExternalService        ResponseCode = "96"
	ErrRecordNotFound         ResponseCode = "97"
	ErrDatabase               ResponseCode = "98"
	ErrSystem                 ResponseCode = "99"

	// EPASS ERROR
	ErrEpActionDenied  ResponseCode = "862"
	ErrEpOrderIDExists ResponseCode = "799"
	ErrEpPlusMoneyEtc  ResponseCode = "857"
	ErrEpBadRequest    ResponseCode = "400"
	ErrEpAmountOver    ResponseCode = "1042"
	ErrEpCommon        ResponseCode = "500"
)

var ResponseMessage = map[string]string{
	"00": "successfully",
	"01": "signature invalid",
	"02": "account-no invalid",
	"03": "transaction expired",
	"04": "account inactive",

	"06": "request data invalid",
	"07": "signature is required",
	"08": "account-no required",
	"09": "merchant-id is required",
	"10": "order-id is required",
	"11": "amount is required",
	"12": "bill-number is required",
	"13": "terminal-id is required",
	"14": "transaction-id is required",
	"15": "status is required",
	"16": "status invalid",
	"17": "status need created",
	"18": "very little money",
	"19": "a lot of money",
	"20": "transaction-id imperative length",
	"21": "account-no imperative length",
	"22": "time invalid format",
	"23": "description imperative length",
	"24": "order-id is exists",
	"25": "order-id invalid",
	"26": "call api of uph update status error",
	"27": "transaction processed",
	"28": "account type invalid",
	"29": "update ok but call uph error",
	"30": "amount update insuffixcient",
	"31": "account status not equal 0",
	"32": "build qr code error",

	"33": "epass transaction required number",
	"34": "epass amount required number",
	"35": "partner invalid",
	"36": "plate number not found",
	"37": "plate type invalid",
	"38": "topup exists",

	"40": "service code require",
	"41": "result code require",

	"95": "execute external timeout",
	"96": "external system error",
	"97": "record not found",
	"98": "database error",
	"99": "system error",
}

func (rc ResponseCode) Code() string {
	return string(rc)
}

func (rc ResponseCode) Message() string {
	if value, ok := ResponseMessage[rc.Code()]; ok {
		return value
	}
	return ""
}

func (rc ResponseCode) MessageC2c() string {
	if value, ok := ResponseMessageC2c[rc.Code()]; ok {
		return value
	}
	if value, ok := ResponseMessage[rc.Code()]; ok {
		return value
	}
	return ""
}

func ParseError(err error) ResponseCode {
	return ResponseCode(err.Error())
}

// status code of c2c and napas
var ()
var (
	ErrMapCodeC2c = map[string]error{
		ErrRecordNotFound.Code():     errors.New(ErrC2cTransNotExits.Code()),
		ErrTransactionExpired.Code(): errors.New(ErrC2cTransNotExpired.Code()),
		ErrAmountMax.Code():          errors.New(ErrC2cLotsOfMoney.Code()),
		// ErrTransProcessed.Code():     errors.New(ErrC2cErrTransProcessed.Code()),
		// ErrLenTranID.Code():          errors.New(ErrC2cLenTranID.Code()),
	}
)

// error - code of c2c
const (
	ErrC2cLotsOfMoney       ResponseCode = "13"
	ErrC2cTransNotExits     ResponseCode = "14"
	ErrC2cTransNotExpired   ResponseCode = "15"
	ErrC2cErrTransProcessed ResponseCode = "27"
	ErrC2cLenTranID         ResponseCode = "20"
)

var ResponseMessageC2c = map[string]string{
	"13": "a lot of money",
	"14": "transaction not exists",
	"15": "transaction expired",
	"27": "transaction processed",
	"20": "transaction-id imperative length",
}
