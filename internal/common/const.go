package common

// indicator prefix QR
const (
	QRPreFix = "QR"
)

// channel service
type ChannelService string

const (
	ChannelC2c ChannelService = "C2C"
)

// postgre database
const (
	CodeDuplicateKey     = "23505"
	NameDuplicateOrderID = "transactions_order_id"
)

var (
	AccountStatusActive = map[string]bool{
		"A": true,
		"N": true,
	}
	AccountType = "700,710,799"
)

// type algorithm
const (
	HmacSha256 = "HMAC_SHA256"
	Sha256     = "SHA256"
)

// constant len
const (
	LenTransactionID = 13
	LenTraceID       = 6
	LenDescription   = 100
	LenAccountNo     = 15
)

// all status of transaction
var (
	StatusCreate  = "CREATED"
	StatusSuccess = "SUCCESS"
	StatusFail    = "FAIL"
	StatusCancel  = "CANCEL"
	StatusExpired = "EXPIRED"
	StatusMapping = map[string]string{
		StatusSuccess: StatusSuccess,
		StatusFail:    StatusFail,
		StatusCancel:  StatusCancel,
	}
)

// parter type
type PartnerName string

const (
	PartnerEpass      PartnerName = "EPASS"
	PartnerEpassApp   PartnerName = "EPASS_APP"
	PartnerEpassNapas PartnerName = "EPASS_NAPAS"
	PartnerQR         PartnerName = "QR"
)

// action type
type ActionTypeAPI string

const (
	PlusMoney   ActionTypeAPI = "PLUS_MONEY"
	QueryResult ActionTypeAPI = "QUERY_RESULT"
	SearchQR    ActionTypeAPI = "SEARCH_QR"
	UpdateQR    ActionTypeAPI = "UPDATE_QR"
	GenQR       ActionTypeAPI = "GEN_QR"
	CancelQR    ActionTypeAPI = "CANCEL_QR"
)
