package dto

type HistoryReqDTO struct {
	Err               error
	FunctionName      string
	RequestId         string
	OrderId           string
	AccountNo         string
	FromAccountNo     string
	MerchantId        string
	TerminalId        string
	Description       string
	BillNumber        string
	Status            string
	AddInfo           string
	ChannelId         string
	ReasonMessage     string
	TransactionId     string
	StatusOfPartner   string
	Partner           string
	RefNo             string
	BatchNumber       string
	NapasAdditional   string
	PartnerErrCode    string
	PartnerErrMessage string
	Amount            float64
}
