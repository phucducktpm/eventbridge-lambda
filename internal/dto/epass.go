package dto

// plus money
type (
	// request
	PlusMoneyReqDTO struct {
		CommonReqDTO
		PlusMoneyReqDataDTO PlusMoneyReqDataDTO `json:"data"`
	}
	PlusMoneyReqDataDTO struct {
		Amount         int64  `json:"amount"`
		DestContractId int64  `json:"destContractId"`
		OrderId        string `json:"orderId"`
	}
	// response
	PlusMoneyResqDTO struct {
		CommonRespDTO
		PlusMoneyResqDataDTO PlusMoneyResqDataDTO `json:"data"`
	}
	PlusMoneyResqDataDTO struct {
		TransId   string `json:"transId"`
		RequestId string `json:"requestId"`
		Balance   int64  `json:"balance"`
	}
)

// query result plus money
type (
	// request
	QueryResultReqDTO struct {
		CommonReqDTO
		QueryResultReqDataDTO QueryResultReqDataDTO `json:"data"`
	}
	QueryResultReqDataDTO struct {
		RequestId string `json:"requestId"`
		TransId   string `json:"transId"`
	}
	// response
	QueryResultResqDTO struct {
		CommonRespDTO
		QueryResultResqDataDTO QueryResultResqDataDTO `json:"data"`
	}
	QueryResultResqDataDTO struct {
		Status string `json:"status"`
	}
)
