package dto

type CustomerDTO struct {
	Partner        string `json:"partner"`
	ChannelId      string `json:"channel_id"`
	CreatedDate    string `json:"created_date"`
	UpdatedDate    string `json:"updated_date"`
	Name           string `json:"name"`
	Cif            string `json:"cif"`
	Phone          string `json:"phone"`
	IssueDate      string `json:"issue_date"`
	CardNumber     string `json:"card_number"`
	CardHolderName string `json:"card_holder_name"`
	Token          string `json:"token"`
	VehicleNumber  string `json:"vehicle_number"`
	PlateType      string `json:"plate_type"`
	Description    string `json:"description"`
	CustType       int64  `json:"cust_type"`
	ContractId     int64  `json:"contract_id"`
}
