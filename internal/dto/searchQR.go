package dto

type SearchQRCodeReqDTO struct {
	CommonReqDTO
	SearchQRCodeReqDataDTO SearchQRCodeReqDataDTO `json:"data"`
}

type SearchQRCodeReqDataDTO struct {
	MerchantId    string  `json:"merchantId"`
	TransactionId string  `json:"transactionId"`
	PlateType     string  `json:"plateType,omitempty"`
	Amount        float64 `json:"-,omitempty"`
	// define field for epass
	// PlateColor  string `json:"plateColor,omitempty"`
	// PlateNumber string `json:"plateNumber,omitempty"`
}

type SearchQRCodeResqDTO struct {
	CommonRespDTO
	SearchQRCodeResqDataDTO SearchQRCodeResqDataDTO `json:"data"`
}

type SearchQRCodeResqDataDTO struct {
	MerchantId    string  `json:"merchantId,omitempty"`
	TerminalId    string  `json:"terminalId,omitempty"`
	OrderId       string  `json:"orderId,omitempty"`
	AccountNo     string  `json:"accountNo,omitempty"`
	AccountName   string  `json:"accountName,omitempty"`
	Description   string  `json:"description,omitempty"`
	BillNumber    string  `json:"billNumber,omitempty"`
	TransactionId string  `json:"transactionId,omitempty"`
	Amount        float64 `json:"amount,omitempty"`
	CreatedDate   string  `json:"createdDate,omitempty"`
	UpdatedDate   string  `json:"updatedDate,omitempty"`
	ReasonMessage string  `json:"reasonMessage,omitempty"`
	Status        string  `json:"status,omitempty"`
	AddInfo       string  `json:"addInfo,omitempty"`
	RefNo         string  `json:"refNo,omitempty"`
	BatchNumber   string  `json:"batchNumber,omitempty"`
	FromAccountNo string  `json:"fromAccountNo,omitempty"`
	ChannelId     string  `json:"channelId,omitempty"`
	// epass response data
	ContractId int64 `json:"contractId,omitempty"`
	CustTypeId int64 `json:"custTypeId,omitempty"`

	// ListData []VehicleAccountDataList `json:"listData,omitempty"`
	// Count    int64                    `json:"count,omitempty"`
}
type (
	VehicleAccountDataList struct {
		CustName         string `json:"custName,omitempty"`
		TopupDescription string `json:"topupDescription,omitempty"`
		ContractId       int64  `json:"contractId,omitempty"`
		CustTypeId       int64  `json:"custTypeId,omitempty"`
		TopupStatus      int64  `json:"topupStatus,omitempty"`
	}
)

type SignatureSearchQRReqDTO struct {
	MerchantId string
	TerminalId string
	SecretKey  string
}

// type ParameterStore struct {
// 	SecretKey           string `json:"secretKey"`
// 	HeaderAuthorization string `json:"headerAuthorization"`
// 	Url                 string `json:"url"`
// }

// type BuildQRCodeReqDTO struct {
// 	Amount        string
// 	TransactionId string
// 	Description   string
// 	BankCode      string
// }

// type BuildQRCodeResqDTO struct {
// 	ImageBase64 string
// }
