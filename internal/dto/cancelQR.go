package dto

// Cancel QR Transaction
type (
	CancelQRReqDTO struct {
		CommonReqDTO
		CancelQRReqDataDTO CancelQRReqDataDTO `json:"data"`
	}

	CancelQRReqDataDTO struct {
		Status        string `json:"status"`
		TransactionId string `json:"transactionId"`
	}
	CancelQRResqDataDTO struct{}
)
