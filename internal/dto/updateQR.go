package dto

import (
	"encoding/json"
	"strconv"
)

type (
	UpdateQRCodeReqDTO struct {
		CommonReqDTO
		UpdateQRCodeReqDataDTO UpdateQRCodeReqDataDTO `json:"data"`
		NapasAdditional        NapasAdditional        `json:"napasAdditional"`
	}
	AmountNumber struct {
	}
	UpdateQRCodeReqDataDTO struct {
		TransactionId string      `json:"transactionId"`
		RefNo         string      `json:"refNo"`
		BatchNumber   string      `json:"batchNumber"`
		Status        string      `json:"status"`
		ReasonMessage string      `json:"reasonMessage"`
		AddInfo       string      `json:"addInfo"`
		FromAccountNo string      `json:"fromAccountNo"`
		OrderId       string      `json:"orderId"`
		Amount        json.Number `json:"amount"`
	}
	NapasAdditional struct {
		// MTI                            string `json:"MTI"`
		// CARD_NUMBER                    string `json:"CARD_NUMBER"`
		// PROCESS_CODE                   string `json:"PROCESS_CODE"`
		// TRANSACTION_AMOUNT             string `json:"TRANSACTION_AMOUNT"`
		// TRANSMISSION_DATE_TIME         string `json:"TRANSMISSION_DATE_TIME"`
		// SYSTEM_TRACE_AUDIT_NUMBER      string `json:"SYSTEM_TRACE_AUDIT_NUMBER"`
		// LOCAL_TRANSACTION_TIME         string `json:"LOCAL_TRANSACTION_TIME"`
		// LOCAL_TRANSACTION_DATE         string `json:"LOCAL_TRANSACTION_DATE"`
		// EXPIRATION_DATE                string `json:"EXPIRATION_DATE"`
		// SETTLEMENT_DATE                string `json:"SETTLEMENT_DATE"`
		// MERCHANT_TYPE                  string `json:"MERCHANT_TYPE"`
		// ACQNG_INST_CNTRY_CODE          string `json:"ACQNG_INST_CNTRY_CODE"`
		// POS_DATA_CODE                  string `json:"POS_DATA_CODE"`
		// POS_COND_CODE                  string `json:"POS_COND_CODE"`
		// ACQUIRING_INSTITITUTION_CODE   string `json:"ACQUIRING_INSTITITUTION_CODE"`
		// FORWARDING_INSTITUTION_ID_CODE string `json:"FORWARDING_INSTITUTION_ID_CODE"`
		// RETRIEVAL_REFERENCE_NUMBER     string `json:"RETRIEVAL_REFERENCE_NUMBER"`
		// AUTH_ID_CODE                   string `json:"AUTH_ID_CODE"`
		// RESPONSE_CODE                  string `json:"RESPONSE_CODE"`
		// CARD_ACCPTR_TERMNL_ID          string `json:"CARD_ACCPTR_TERMNL_ID"`
		// CARD_ACCPTR_ID_CODE            string `json:"CARD_ACCPTR_ID_CODE"`
		// CARD_ACCPTR_NAME_LOCATION      string `json:"CARD_ACCPTR_NAME_LOCATION"`
		// ADDITIONAL_PRIVATE_DATA        string `json:"ADDITIONAL_PRIVATE_DATA"`
		// CURRENCY_CODE                  string `json:"CURRENCY_CODE"`
		// SELF_DEFINED_FIELD             string `json:"SELF_DEFINED_FIELD"`
		// SERVICE_CODE                   string `json:"SERVICE_CODE"`
		// TRANS_REF_NUM                  string `json:"TRANS_REF_NUM"`
		// ADDITIONAL_AMOUNT              string `json:"ADDITIONAL_AMOUNT"`
		// SOURCE_ACCOUNT_NUMBER          string `json:"SOURCE_ACCOUNT_NUMBER"`
		// DESTINATION_CARD_NUMBER        string `json:"DESTINATION_CARD_NUMBER"`
		// NARATION                       string `json:"NARATION"`
		// MAC                            string `json:"MAC"`

		MTI                            string `json:"mti"`
		CARD_NUMBER                    string `json:"cardNumber"`
		PROCESS_CODE                   string `json:"processCode"`
		TRANSACTION_AMOUNT             string `json:"transactionAmount"`
		TRANSMISSION_DATE_TIME         string `json:"transmissionDateTime"`
		SYSTEM_TRACE_AUDIT_NUMBER      string `json:"systemTraceAuditNumber"`
		LOCAL_TRANSACTION_TIME         string `json:"localTransactionTime"`
		LOCAL_TRANSACTION_DATE         string `json:"localTransactionDate"`
		EXPIRATION_DATE                string `json:"expirationDate"`
		SETTLEMENT_DATE                string `json:"settlementDate"`
		MERCHANT_TYPE                  string `json:"merchantType"`
		ACQNG_INST_CNTRY_CODE          string `json:"acqngInstCntryCode"`
		POS_DATA_CODE                  string `json:"posDataCode"`
		POS_COND_CODE                  string `json:"poscondCode"`
		ACQUIRING_INSTITITUTION_CODE   string `json:"acquiringInsTitutionCode"`
		FORWARDING_INSTITUTION_ID_CODE string `json:"forwardingInstitutionIdCode"`
		RETRIEVAL_REFERENCE_NUMBER     string `json:"retrievalReferenceNumber"`
		AUTH_ID_CODE                   string `json:"authIdCode"`
		RESPONSE_CODE                  string `json:"responseCode"`
		CARD_ACCPTR_TERMNL_ID          string `json:"cardAccptrTermnlId"`
		CARD_ACCPTR_ID_CODE            string `json:"cardAccptrIdCode"`
		CARD_ACCPTR_NAME_LOCATION      string `json:"cardAccptfNameLocation"`
		ADDITIONAL_PRIVATE_DATA        string `json:"additionalPrivateData"`
		CURRENCY_CODE                  string `json:"currencyCode"`
		SELF_DEFINED_FIELD             string `json:"selfDefinedField"`
		SERVICE_CODE                   string `json:"serviceCode"`
		TRANS_REF_NUM                  string `json:"transRefNum"`
		ADDITIONAL_AMOUNT              string `json:"additionalAmount"`
		SOURCE_ACCOUNT_NUMBER          string `json:"sourceAccountNumber"`
		DESTINATION_CARD_NUMBER        string `json:"destinationCardNumber"`
		NARATION                       string `json:"naration"`
		MAC                            string `json:"mac"`
	}
)

type TypeWrapper float64

func (w *TypeWrapper) UnmarshalJSON(data []byte) (err error) {
	zip, err := strconv.ParseFloat(string(data), 64)
	if err == nil {
		*w = TypeWrapper(zip)
		return nil
	}
	var str string
	err = json.Unmarshal(data, &str)
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(str), w)
}

type UpdateQRCodeResqDTO struct {
	// CommonRespDTO
	UpdateQRCodeResqDataDTO UpdateQRCodeResqDataDTO `json:"data,omitempty"`
}

type UpdateQRCodeResqDataDTO struct {
	TraceId       string `json:"trace_id,omitempty"`
	PartnerStatus string `json:"partner_status,omitempty"`
	TransId       string `json:"transId,omitempty"`
	RequestId     string `json:"requestId,omitempty"`
	Balance       int64  `json:"balance,omitempty"`
}

type CreateBillPaymentDTO struct {
	ChannelId     string `json:"channelId"`
	RequestId     string `json:"requestId"`
	RequestTime   string `json:"requestTime"`
	Signature     string `json:"signature"`
	TransactionId string `json:"transactionId"`
	RefNo         string `json:"refNo"`
	BatchNumber   string `json:"batchNumber"`
	Status        string `json:"status"`
	ReasonMessage string `json:"reasonMessage"`
	AddInfo       string `json:"addInfo"`
	FromAccountNo string `json:"fromAccountNo"`
	Amount        string `json:"amount"`
	OrderID       string `json:"orderId"`
	FeeValue      string `json:"feeValue"`
	VatValue      string `json:"vatValue"`
	CustomerName  string `json:"customerName"`
	PlateNumber   string `json:"plateNumber"`
}
