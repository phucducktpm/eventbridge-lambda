package dto

type GenQRCodeReqDTO struct {
	CommonReqDTO
	GenQRCodeReqDataDTO GenQRCodeReqDataDTO `json:"data"`
}

type GenQRCodeReqDataDTO struct {
	MerchantId    string  `json:"merchantId"`
	TerminalId    string  `json:"terminalId"`
	OrderId       string  `json:"orderId"`
	AccountNo     string  `json:"accountNo"`
	Description   string  `json:"description"`
	BillNumber    string  `json:"billNumber"`
	TransactionId string  `json:"transactionId"`
	Amount        float64 `json:"amount"`
}

type GenQRCodeResqDTO struct {
	CommonRespDTO
	GenQRCodeResqDataDTO GenQRCodeResqDataDTO `json:"data"`
}

type GenQRCodeResqDataDTO struct {
	ImageStr      string `json:"imageStr"`
	Content       string `json:"content"`
	AccountNo     string `json:"accountNo"`
	TransactionId string `json:"transactionId"`
}

type SignatureGenQRReqDTO struct {
	MerchantId string
	TerminalId string
	SecretKey  string
}
type ParameterStore struct {
	SecretKey           string `json:"secretKey"`
	HeaderAuthorization string `json:"headerAuthorization"`
	Url                 string `json:"url"`
	SecretKeyExternal   string `json:"secretKeyExternal"`
}

type BuildQRCodeReqDTO struct {
	Amount        string
	TransactionId string
	Description   string
	BankCode      string
}

type BuildQRCodeResqDTO struct {
	ImageBase64 string
}

// merchant info
type MerchantInfo struct {
	MerchantName string `json:"merchantName" mapstructure:"merchantName"`
	MerchantCode string `json:"merchantCode" mapstructure:"merchantCode"`
}
