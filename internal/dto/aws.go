package dto

type AwsSessionReq struct {
	CustomEndpoint bool
	AssumeRole     bool
}

type AwsParameterStoreReq struct {
	Key string
}
