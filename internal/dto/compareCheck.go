package dto

type (
	CompareOther struct {
		FromAccount string
	}
	CompareCheckReqDTO struct {
		CommonReqDTO
		CompareCheckeReqDataDTO CompareCheckReqDataDTO `json:"data"`
	}
	CompareCheckReqDataDTO struct {
		PartnerId   string   `json:"partnerId"`
		OrderId     string   `json:"orderId"`
		ContractId  string   `json:"contractId"`
		PlateNumber string   `json:"plateNumber"`
		FromDate    string   `json:"fromDate"`
		Status      string   `json:"status"`
		ToDate      string   `json:"toDate"`
		ResultCode  string   `json:"resultCode"`
		ServiceCode []string `json:"serviceCode"`
	}

	CompareCheckResqDataDTO struct {
		ChannelPayment string  `json:"channelPayment"`
		CodeHandle     string  `json:"codeHandle"`
		CodeDetail     string  `json:"codeDetail"`
		InvoiceCode    string  `json:"invoiceCode"`
		PartnerId      string  `json:"partnerId"`
		RefNo          string  `json:"RefNo"`
		DateReceive    string  `json:"DateReceive"`   // YYYYMMDD
		TimeReceive    string  `json:"TimeReceive"`   // hh24mmss
		DateExecute    string  `json:"DateExecute"`   // YYYYMMDD
		TimeExecute    string  `json:"TimeExecute"`   // hh24mmss
		AccountNoPlus  string  `json:"AccountNoPlus"` // ghi co
		AccountNoSub   string  `json:"AccountNoSub"`  // ghi no
		CardNumber     string  `json:"CardNumber"`
		CardAccept     string  `json:"CardAccept"`
		ResultCompare  string  `json:"ResultCompare"`
		PartnerRequire string  `json:"PartnerRequire"`
		PaymentType    string  `json:"PaymentType"`
		AddInfo        string  `json:"AddInfo"`
		OrderId        string  `json:"orderId"`
		Amount         float64 `json:"amount"`
		AmountType     string  `json:"amountType"`

		// Date           string `json:"date"`
		// Cif            string `json:"cif"`
		// AccountName    string `json:"accountName"`
		// PlateNumber    string `json:"plateNumber"`
		// ContractId     string `json:"contractId"`
		// ContractName   string `json:"contractName"`
		// AccountVehicle string `json:"accountVehicle"`
		// Balance       float64 `json:"balance"`
		// Status        string  `json:"status"`
		// CancelDate    string  `json:"cancelDate"`
		// OrderIdCancel string  `json:"orderIdCancel"`
	}
)
