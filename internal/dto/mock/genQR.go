package mock

import "scheduler-management/internal/dto"

func GenQRReq() dto.GenQRCodeReqDTO {
	return dto.GenQRCodeReqDTO{
		CommonReqDTO: dto.CommonReqDTO{
			RequestId: "sdrfsdfdsf",
			Signature: "3170f7d2f5803c2eae3973ae68fca636fb760cb3c29ad545fb3024c6935b7c67",
		},
		GenQRCodeReqDataDTO: dto.GenQRCodeReqDataDTO{
			MerchantId:  "000000000000000",
			TerminalId:  "5555555",
			OrderId:     "06ca91fe9f-91df-4813-3122-cf21b2d01167",
			AccountNo:   "002704070000189",
			Description: "this",
			BillNumber:  "888888",
			Amount:      112111,
		},
	}
}
func GenQRReq1() dto.GenQRCodeReqDTO {
	return dto.GenQRCodeReqDTO{
		CommonReqDTO: dto.CommonReqDTO{
			RequestId: "sdf",
			Signature: "sdafasdf",
		},
		GenQRCodeReqDataDTO: dto.GenQRCodeReqDataDTO{
			MerchantId:  "123sdfsdf",
			TerminalId:  "sdfwerwe",
			OrderId:     "123sdfsdfa",
			AccountNo:   "068704070000489",
			Description: "ducnp dev test",
			BillNumber:  "sdfidfg893",
			Amount:      100000,
		},
	}
}
