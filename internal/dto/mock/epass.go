package mock

import (
	"scheduler-management/internal/dto"

	"github.com/google/uuid"
)

func PlusMoneyMock() dto.PlusMoneyReqDTO {
	return dto.PlusMoneyReqDTO{
		PlusMoneyReqDataDTO: dto.PlusMoneyReqDataDTO{
			Amount:         10000,
			DestContractId: 98,
			OrderId:        uuid.NewString(),
		},
	}
}

func ResultPlusMoneyMock() dto.QueryResultReqDTO {
	return dto.QueryResultReqDTO{
		QueryResultReqDataDTO: dto.QueryResultReqDataDTO{
			RequestId: "220822018229",
			TransId:   "",
		},
	}
}

func CompareCheck() dto.CompareCheckReqDTO {
	return dto.CompareCheckReqDTO{
		CompareCheckeReqDataDTO: dto.CompareCheckReqDataDTO{
			FromDate:    "2023-03-15",
			ToDate:      "2023-03-20",
			PartnerId:   "EPASS",
			ResultCode:  "00000",
			ServiceCode: []string{"EPASS", "EPASS_APP", "EPASS_NAPAS"},
		},
	}
}
