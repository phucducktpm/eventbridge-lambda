package mock

import "scheduler-management/internal/dto"

func SearchQRResponse() dto.SearchQRCodeResqDataDTO {
	return dto.SearchQRCodeResqDataDTO{
		MerchantId:    "18289345028",
		TerminalId:    "che2378349294ck1",
		OrderId:       "123456123",
		AccountNo:     "068704070000489",
		Description:   "ducnp dev test",
		BillNumber:    "sdfidfg893",
		TransactionId: "",
		Amount:        1000,
	}
}

func SearchQRRequest() dto.SearchQRCodeReqDTO {
	return dto.SearchQRCodeReqDTO{
		CommonReqDTO: dto.CommonReqDTO{
			ChannelId:   "C2C",
			Signature:   "945ad6b65728810e804b713ab31b3030d1798f7723df4b2a421b064947c1dee9",
			RequestId:   "",
			RequestTime: "",
		},
		SearchQRCodeReqDataDTO: dto.SearchQRCodeReqDataDTO{
			MerchantId:    "",
			TransactionId: "EPASS30G22216",
		},
	}
}
func SearchQRRequest1() dto.SearchQRCodeReqDTO {
	return dto.SearchQRCodeReqDTO{
		CommonReqDTO: dto.CommonReqDTO{
			Signature:   "sdfsd",
			RequestId:   "",
			RequestTime: "",
		},
		SearchQRCodeReqDataDTO: dto.SearchQRCodeReqDataDTO{
			MerchantId:    "fsdfsd",
			TransactionId: "FmVWMlUIsdfasdfasdT9kzswEsU",
		},
	}
}
