package mock

import (
	"scheduler-management/internal/common"
	"scheduler-management/internal/dto"
)

func UpdateStatusRequest() dto.UpdateQRCodeReqDTO {
	return dto.UpdateQRCodeReqDTO{
		CommonReqDTO: dto.CommonReqDTO{
			ChannelId:   "C2C",
			RequestId:   "sdfsdf",
			RequestTime: "",
			Signature:   "06e775f506fdbbc5b464f69866629fe5d621c8c30d71f24b3ea58c00c7c1498a",
		},
		UpdateQRCodeReqDataDTO: dto.UpdateQRCodeReqDataDTO{
			// MerchantId:    "18289345028",
			// TerminalId:    "che2378349294ck1",
			TransactionId: "EPASS1002009",
			// OrderId:       "123456123",
			Status:        common.StatusSuccess,
			ReasonMessage: "ok",
			RefNo:         "2312",
			BatchNumber:   "243234",
			AddInfo:       "add-info",
			Amount:        "11000",
			OrderId:       "f4b59469-f30d-4309-9145-49adbb8dc6d2",
			// OrderId:       "220822118100",
		},
	}
}

func UpdateStatusRequest1() dto.UpdateQRCodeReqDTO {
	return dto.UpdateQRCodeReqDTO{
		CommonReqDTO: dto.CommonReqDTO{
			RequestId:   "sdfsdf",
			RequestTime: "",
			Signature:   "25092e74ffe5d229a07b58bf4411888c9f3e9eaf1399a2db84f50429444c11a4",
		},
		UpdateQRCodeReqDataDTO: dto.UpdateQRCodeReqDataDTO{
			// MerchantId:    "sdf",
			// TerminalId:    "fdsf",
			TransactionId: "sdf",
			// OrderId:       "123456123",
			Status:        common.StatusFail,
			ReasonMessage: "ok",
		},
	}
}

func UpdateStatusResponse() dto.UpdateQRCodeResqDTO {
	return dto.UpdateQRCodeResqDTO{
		// CommonRespDTO:           dto.CommonRespDTO{},
		UpdateQRCodeResqDataDTO: dto.UpdateQRCodeResqDataDTO{},
	}
}
