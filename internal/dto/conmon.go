package dto

type CommonReqDTO struct {
	ChannelId   string `json:"channelId"`
	RequestId   string `json:"requestId"`
	RequestTime string `json:"requestTime"`
	Signature   string `json:"signature"`
}

type CommonRespDTO struct {
	RequestId       string `json:"requestId"`
	ResponseCode    string `json:"responseCode"`
	ResponseTime    string `json:"responseTime"`
	ResponseMessage string `json:"responseMessage"`
}

type DataCheckWhiteList struct {
	AccountNo   string
	CreatedDate string
	UpdatedDate string
	RequestID   string
}
