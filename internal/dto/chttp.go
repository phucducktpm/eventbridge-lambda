package dto

type ClientHttpRequest struct {
	Body        interface{}
	Method      string
	Url         string
	Header      map[string]string
	ContentType string
	Query       string
}
