package usecase

// import (
// 	"context"
// 	"errors"
// 	"strings"

// 	"scheduler-management/internal/adapter"
// 	billing "scheduler-management/internal/adapter/billingHub"
// 	"scheduler-management/internal/adapter/epass"
// 	"scheduler-management/internal/common"
// 	"scheduler-management/internal/dto"
// 	"scheduler-management/internal/repository"
// 	"scheduler-management/pkg/chttp"
// )

// // service
// type Usecase struct {
// 	TransactionQRUsecase IbftVietQRUsecase
// }

// func InitUsecase(ctx context.Context, repo repository.Repository) (*Usecase, error) {
// 	chttp := chttp.NewClientHttp(0)
// 	his := NewHistoryUsecase(repo.NewHistory())

// 	ibUse := NewIbftVietQRUsecase(repo.NewTransaction(), nil, nil, his, adapter.NewVietQRCodeAdapter(chttp), adapter.NewEsbAdapter(chttp), adapter.NewUPHAdapter(chttp), nil, nil)
// 	return &Usecase{
// 		TransactionQRUsecase: ibUse,
// 	}, nil
// }

// // lambda instance
// type IbftVietQRUsecase interface {
// 	Gen(ctx context.Context, reqDTO dto.GenQRCodeReqDTO) (dto.GenQRCodeResqDataDTO, error)
// 	CancelQR(ctx context.Context, reqDTO dto.CancelQRReqDTO) (dto.CancelQRResqDataDTO, error)
// 	Search(ctx context.Context, reqDTO dto.SearchQRCodeReqDTO) (dto.SearchQRCodeResqDataDTO, error)
// 	Update(ctx context.Context, reqDTO dto.UpdateQRCodeReqDTO) (dto.UpdateQRCodeResqDataDTO, error)
// 	CompareCheck(ctx context.Context, reqDTO dto.CompareCheckReqDTO) ([]dto.CompareCheckResqDataDTO, error)
// }

// type ibftVietQRUsecase struct {
// 	tranRepo       repository.TransactionRepo
// 	partnerRepo    repository.PartnerRepo
// 	customerRepo   repository.CustomerRepo
// 	historyUsecase HistoryUsecase
// 	vietQRAdapter  adapter.VietQRCodeAdapter
// 	esbAdapter     adapter.EsbAdapter
// 	uphAdapter     adapter.UPHAdapter
// 	epassAdapter   epass.EPassdapter
// 	billAdapter    billing.BillingAdapter
// }

// func NewIbftVietQRUsecase(
// 	tran repository.TransactionRepo,
// 	partner repository.PartnerRepo,
// 	customer repository.CustomerRepo,
// 	hisUsecase HistoryUsecase,
// 	vietQRAdapter adapter.VietQRCodeAdapter,
// 	esbAdapter adapter.EsbAdapter,
// 	uphAdapter adapter.UPHAdapter,
// 	epassAdapter epass.EPassdapter,
// 	billAdapter billing.BillingAdapter,
// ) IbftVietQRUsecase {
// 	return &ibftVietQRUsecase{
// 		tranRepo:       tran,
// 		partnerRepo:    partner,
// 		customerRepo:   customer,
// 		historyUsecase: hisUsecase,
// 		vietQRAdapter:  vietQRAdapter,
// 		esbAdapter:     esbAdapter,
// 		uphAdapter:     uphAdapter,
// 		epassAdapter:   epassAdapter,
// 		billAdapter:    billAdapter,
// 	}
// }

// // switch choice partner
// func (*ibftVietQRUsecase) choiceParnerName(ctx context.Context, transID string) (string, error) {
// 	if strings.HasPrefix(transID, string(common.PartnerEpass)) {
// 		return string(common.PartnerEpass), nil
// 	} else if strings.HasPrefix(transID, string(common.PartnerQR)) {
// 		return string(common.PartnerQR), nil
// 	}
// 	return "", errors.New(common.ErrPartnerInvalid.Code())
// }

// func PartnerName(transId string) (string, error) {
// 	if strings.HasPrefix(transId, string(common.PartnerEpass)) {
// 		return string(common.PartnerEpass), nil
// 	} else if strings.HasPrefix(transId, string(common.PartnerQR)) {
// 		return string(common.PartnerQR), nil
// 	}
// 	return "", errors.New(common.ErrPartnerInvalid.Code())
// }
