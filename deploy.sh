#!/bin/sh
envStage=$1
functionName=$2

# set env stage default dev
if ([ -z "$envStage" ])
then 
    envStage="dev"
fi

# Safety Checks
if ([ "$envStage" != "dev" ] && [ "$envStage" != "uat" ] && [ "$envStage" != "pro" ]  )
then
    echo "Please provide arguments: Usage: ./deploy.sh [ENV_TO_DEPLOY]"
    echo "Supported environments: dev / uat / pro "
    exit 1
fi
    echo "Deployment started for environment: " $envStage


if [ "$envStage" != "dev"  ]; then
    echo "config go env"
    export PATH=$PATH:/usr/local/go/bin/
    . ~/.nvm/nvm.sh
    nvm use v12
    node -e "console.log('Running Node.js ' + process.version)"
    npm i serverless-plugin-ifelse --save-dev

fi

#Deploying the lambdas via serverless

echo "Starging deploying lambda"

if ([ -z "$functionName" ])
then
    echo "Clean up and all function"
    rm bin/*

    make clean 
    make build

    echo "deploy all function in stage $envStage"
    serverless deploy --stage $envStage
else
    echo "Clean up and rebuild function $functionName"
    make clean
    make build
    echo "deploy only function $functionName in stage $envStage"
    sls deploy  --stage $envStage -f $functionName

fi

echo "End deploying successfully"

exit 0