package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(scheduler)
}

func scheduler(ctx context.Context, evt events.CloudWatchEvent) error {
	fmt.Println(evt.Resources)
	fmt.Println(evt.Source)
	fmt.Println(evt.Detail)
	fmt.Println(evt.DetailType)
	return nil
}
