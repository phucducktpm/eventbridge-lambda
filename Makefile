.PHONY: build clean deploy

build:
	env GOARCH=amd64 CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" -o bin/scheduler functions/scheduler/scheduler.go

clean:
	rm -rf ./bin

deploy: clean build
	sls deploy --verbose

lambda:
	@mkdir -p functions
	@mkdir -p envs
	@mkdir -p cmd
	@mkdir -p cmd/server
	@mkdir -p cmd/worker
	@mkdir -p scripts
	@mkdir -p third_party
	@mkdir -p internal
	@mkdir -p internal/usecase
	@mkdir -p pkg
	@mkdir -p sql

mock: 
	mockgen -source=internal/adapter/vietQR.go -destination=internal/adapter/mock/vietQR.go -package mockAdapter
	mockgen -source=internal/usecase/gen.go -destination=internal/usecase/mock/gen.go -package mockUsecase
	mockgen -source=internal/repository/transaction.go -destination=internal/repository/mock/transaction.go  -package mockRepository
gen:
	swag init  --parseInternal internal -d internal/api -g api.go