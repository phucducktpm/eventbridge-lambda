package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

// load all env from file .env,
func LoadEnv(envP string) error {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	log.Default().Println("Getwd", dir)
	err = godotenv.Load(envP)
	if err != nil {
		panic(err)
	}
	return nil
}
