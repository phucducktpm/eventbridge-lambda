package pkg

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"scheduler-management/internal/common"
)

/*
gen signature with plain-text and secretKey,
currenly only support algorithm sha256
*/
func BuildSignature(secretkey, plainText, algorithm string) string {
	var (
		signature string
	)

	switch algorithm {
	case common.Sha256:
		sum := sha256.Sum256([]byte(plainText))
		signature = fmt.Sprintf("%x", sum)
	case common.HmacSha256:
		mac := hmac.New(sha256.New, []byte(secretkey))
		mac.Write([]byte(plainText))
		expectedMAC := mac.Sum(nil)
		signature = hex.EncodeToString(expectedMAC)
	default:
		mac := hmac.New(sha256.New, []byte(secretkey))
		mac.Write([]byte(plainText))
		expectedMAC := mac.Sum(nil)
		signature = hex.EncodeToString(expectedMAC)
	}
	return signature
}
