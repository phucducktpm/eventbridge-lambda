package logger

import (
	"flag"
	"fmt"
	"os"
	"sync"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	// LogWriter *log.Logger
	once sync.Once
)

func InitLog() {
	once.Do(func() {
		zap.S().Info("ssdfsdf")

		pwd, err := os.Getwd()
		if err != nil {
			panic(err)
		}
		fmt.Println("pwd log", pwd)
		flag.Parse()
		file, err := os.OpenFile("sdfsdf.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			panic(err)
		}
		// LogWriter = log.New(file, "", log.LstdFlags|log.Lshortfile)

		logInit(true, file)
	})
	Newlogger("development", "debug", "")

}

func logInit(d bool, f *os.File) *zap.SugaredLogger {

	pe := zap.NewProductionEncoderConfig()

	fileEncoder := zapcore.NewJSONEncoder(pe)

	pe.EncodeTime = zapcore.ISO8601TimeEncoder
	consoleEncoder := zapcore.NewConsoleEncoder(pe)

	level := zapcore.DebugLevel

	core := zapcore.NewTee(
		zapcore.NewCore(fileEncoder, zapcore.AddSync(f), level),
		zapcore.NewCore(consoleEncoder, zapcore.AddSync(os.Stdout), level),
	)

	l := zap.New(core)
	return l.Sugar()
}
