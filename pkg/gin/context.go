package gin

import (
	"net/http"
	"scheduler-management/pkg"
	"time"

	"github.com/gin-gonic/gin"
)

type ContextGin struct {
	*gin.Context
}
type ResponseData struct {
	ResponseCode    string      `json:"responseCode"`
	ResponseMessage string      `json:"responseMessage,omitempty"`
	ResponseId      string      `json:"responseId,omitempty"`
	ResponseTime    string      `json:"responseTime,omitempty"`
	Data            interface{} `json:"data,omitempty"`
}
type HandlerFunc func(ctx *ContextGin)

func WithContext(handler HandlerFunc) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		handler(&ContextGin{
			ctx,
		})
	}
}

func (c *ContextGin) BadRequest(err error) {
	resp := ResponseData{}
	c.responseJson(http.StatusBadRequest, resp)
}

func (c *ContextGin) BadLogic(err error) {
	resp := ResponseData{
		ResponseCode:    pkg.ParseError(err).Code(),
		ResponseMessage: pkg.ParseError(err).Message(),
		ResponseTime:    time.Now().Format("2006-01-02T15:04:05.000-07:00"),
	}
	c.responseJson(http.StatusOK, resp)
}

func (c *ContextGin) OKResponse(data interface{}) {
	resp := ResponseData{
		ResponseCode:    "00",
		ResponseMessage: "successfully",
		ResponseTime:    time.Now().Format("2006-01-02T15:04:05.000-07:00"),
	}
	if data != nil {
		resp.Data = data
	}
	c.responseJson(http.StatusOK, resp)
}

func (c *ContextGin) responseJson(code int, data interface{}) {
	c.JSON(code, data)
	if code != http.StatusOK {
		c.Abort()
	}
}

func (c *ContextGin) TokenNotFound() {
}
