package aws

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"scheduler-management/internal/dto"

	"github.com/aws/aws-sdk-go/service/ssm"
)

func GetParameter(name *string, req dto.AwsSessionReq) (results *ssm.GetParameterOutput, err error) {
	session := CreateSession(dto.AwsSessionReq{
		CustomEndpoint: req.CustomEndpoint,
		AssumeRole:     req.AssumeRole,
	})
	if name == nil {
		return results, fmt.Errorf("get parameter required field name is required")
	}
	return ssm.New(session).GetParameter(&ssm.GetParameterInput{
		Name: name,
	})
}

// get aws parameter-store
func GetSecretKey(ctx context.Context, req dto.AwsSessionReq, key string) (dto.ParameterStore, error) {
	var (
		param = dto.ParameterStore{}
		err   error
	)
	paramOut, err := GetParameter(&key, dto.AwsSessionReq{
		CustomEndpoint: req.CustomEndpoint,
		AssumeRole:     req.AssumeRole,
	})
	if err != nil {
		return param, err
	}
	value := *paramOut.Parameter.Value
	if value == "" {
		return param, fmt.Errorf("value of parameter %s value empty", key)
	}
	err = json.Unmarshal([]byte(value), &param)
	if err != nil {
		return param, fmt.Errorf("json Unmarshal of partner %s, err %s", key, err)
	}

	sec, err := base64.StdEncoding.DecodeString(param.SecretKey)
	if err != nil {
		return param, fmt.Errorf("decode-string secretkey err %s", err)
	}
	param.SecretKey = string(sec)
	// fmt.Println("secretKey:", param.SecretKey)

	return param, err
}
