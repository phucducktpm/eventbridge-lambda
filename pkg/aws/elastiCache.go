package aws

import (
	"context"
	"crypto/tls"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elasticache"
	"github.com/redis/go-redis/v9"
)

func MainElastiCache() {
	ctx := context.Background()
	ElastiCaheClient(nil)

	a := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs:    []string{"rl-apse1-az12-uat-sso.az1bab.ng.0001.apse1.cache.amazonaws.com:6379"},
		Password: "P@ssw0rd@)@#2023", // no password set
		Username: "thanhtt4",
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	})
	_, err := a.Ping(ctx).Result()
	if err != nil {
		panic(err)
	}
	h1, h2 := a.Set(ctx, "ssdf", "sdfsd", time.Hour).Result()
	fmt.Println(h1, h2)
	fmt.Println("MainElastiCache")
	rdb := redis.NewClient(&redis.Options{
		Addr:     "rl-apse1-az12-uat-sso.az1bab.ng.0001.apse1.cache.amazonaws.com:6379",
		Password: "P@ssw0rd@)@#2023", // no password set
		DB:       0,                  // use default DB
		Username: "thanhtt4",
	})
	fmt.Println(rdb)
	_, err = rdb.Ping(ctx).Result()
	if err != nil {
		panic(err)
	}
}
func Session() *session.Session {
	return session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
}

func ElastiCaheClient(s *session.Session) *elasticache.ElastiCache {
	if s == nil {
		s = Session()
	}
	ec := elasticache.New(s)
	// a, b := ec.CreateUserGroupRequest(&elasticache.CreateUserGroupInput{})
	return ec
}
