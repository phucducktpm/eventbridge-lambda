package aws

import (
	"scheduler-management/internal/dto"

	"github.com/aws/aws-sdk-go/aws/session"
)

// create session aws
func CreateSession(req dto.AwsSessionReq) *session.Session {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config:            GetConfig(req.CustomEndpoint),
	}))
	return sess
}
