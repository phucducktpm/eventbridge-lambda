package aws

import (
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
)

func GetRoleArn() string {
	return os.Getenv("ASSUME_ROLE_ARN")
}

func GetRegion() string {
	region := os.Getenv("CUSTOM_AWS_REGION")
	return region
}

// build config call aws
func GetConfig(customEndpoint bool) aws.Config {
	config := aws.NewConfig()
	config.Region = aws.String(GetRegion())
	if customEndpoint {
		log.Default().Println("custom endpoint")
		myCustomResolver := func(service, region string, optFns ...func(*endpoints.Options)) (endpoints.ResolvedEndpoint, error) {
			if service == endpoints.S3ServiceID {
				url := os.Getenv("S3_ENDPOINT_RESOLVE")
				signingRegion := os.Getenv("S3_ENDPOINT_SIGNING_REGION")
				log.Default().Println("service s3, endpoint resolve, region", url, signingRegion)
				return endpoints.ResolvedEndpoint{
					URL:           url,
					SigningRegion: signingRegion,
				}, nil
			}
			if service == endpoints.DynamodbServiceID {
				url := os.Getenv("DYNAMO_ENDPOINT_RESOLVE")
				signingRegion := os.Getenv("DYNAMO_ENDPOINT_SIGNING_REGION")
				log.Default().Println("service dynamo, endpoint resolve, region", url, signingRegion)
				return endpoints.ResolvedEndpoint{
					URL:           url,
					SigningRegion: signingRegion,
				}, nil
			}
			if service == endpoints.RekognitionServiceID {
				url := os.Getenv("REKOGNITION_ENDPOINT_RESOLVE")
				signingRegion := os.Getenv("REKOGNITION_ENDPOINT_SIGNING_REGION")
				log.Default().Println("service rekognition, endpoint resolve, region", url, signingRegion)
				return endpoints.ResolvedEndpoint{
					URL:           url,
					SigningRegion: signingRegion,
				}, nil
			}
			//
			return endpoints.DefaultResolver().EndpointFor(service, region, optFns...)
		}
		config.EndpointResolver = endpoints.ResolverFunc(myCustomResolver)
	}
	return *config
}
