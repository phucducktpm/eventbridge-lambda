package pkg

type ResponseCode string

const (
	Successfully          ResponseCode = "00"
	ErrSignatureInvalid   ResponseCode = "01"
	ErrAccountNoInvalid   ResponseCode = "02"
	ErrTransactionExpired ResponseCode = "03"
	ErrAccountInactive    ResponseCode = "04"

	BadRequestJson           ResponseCode = "06"
	ErrSignatureRequired     ResponseCode = "07"
	ErrAccountNoRequired     ResponseCode = "08"
	ErrMerchantRequired      ResponseCode = "09"
	ErrOrderIDRequired       ResponseCode = "10"
	ErrAmountRequired        ResponseCode = "11"
	ErrBillNumberRequired    ResponseCode = "12"
	ErrTerminalIDRequired    ResponseCode = "13"
	ErrTransactionIDRequired ResponseCode = "14"
	ErrStatusRequired        ResponseCode = "15"
	ErrStatusInvalid         ResponseCode = "16"
	ErrStatusNeedCreated     ResponseCode = "17"
	ErrAmountMin             ResponseCode = "18"
	ErrAmountMax             ResponseCode = "19"
	ErrLenTranID             ResponseCode = "20"
	ErrLenAccNo              ResponseCode = "21"

	ErrRecordNotFound ResponseCode = "97"
	ErrDatabase       ResponseCode = "98"
	ErrSystem         ResponseCode = "99"
)

var ResponseMessage = map[string]string{
	"00": "successfully",
	"01": "signature invalid",
	"02": "account-no invalid",
	"03": "transaction expired",
	"04": "account inactive",

	"06": "request data invalid",
	"07": "signature is required",
	"08": "account-no required",
	"09": "merchant-id is required",
	"10": "order-id is required",
	"11": "amount is required",
	"12": "bill-number is required",
	"13": "terminal-id is required",
	"14": "transaction-id is required",
	"15": "status is required",
	"16": "status invalid",
	"17": "status need created",
	"18": "very little money",
	"19": "a lot of money",
	"20": "transaction-id imperative length",
	"21": "account-no imperative length",

	"97": "record not found",
	"98": "database error",
	"99": "system error",
}

func (rc ResponseCode) Code() string {
	return string(rc)
}

func (rc ResponseCode) Message() string {
	if value, ok := ResponseMessage[rc.Code()]; ok {
		return value
	}
	return ""
}

func ParseError(err error) ResponseCode {
	return ResponseCode(err.Error())
}
