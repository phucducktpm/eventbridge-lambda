package db

import (
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"scheduler-management/internal/common"
	"scheduler-management/pkg/utils"

	"github.com/jackc/pgconn"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Postgres struct {
	Username    string `yaml:"username" mapstructure:"username"`
	Password    string `yaml:"password" mapstructure:"password"`
	Database    string `yaml:"database" mapstructure:"database"`
	Host        string `yaml:"host" mapstructure:"host"`
	Port        int    `yaml:"port" mapstructure:"port"`
	Schema      string `yaml:"schema" mapstructure:"schema"`
	MaxIdleConn int    `yaml:"max_idle_conn" mapstructure:"max_idle_conn"`
	MaxOpenConn int    `yaml:"max_open_conn" mapstructure:"max_open_conn"`
}

// read environment of databse
func loadConfig() Postgres {
	user, _ := utils.GetenvStr("DB_USER")
	dbpass, _ := utils.GetenvStr("DB_PASS")
	dbhost, _ := utils.GetenvStr("DB_HOST")
	dbport, _ := utils.GetenvInt("DB_PORT")
	dbservice, _ := utils.GetenvStr("DB_SERVICE")
	idleC, _ := utils.GetenvInt("DB_MAX_IDLE_CONN")
	openC, _ := utils.GetenvInt("DB_MAX_OPEN_CONN")
	schema, _ := utils.GetenvStr("DB_SCHEMA")
	password, _ := base64.StdEncoding.DecodeString(dbpass)
	return Postgres{
		Username:    user,
		Password:    string(password),
		Database:    dbservice,
		Host:        dbhost,
		Port:        dbport,
		MaxIdleConn: idleC,
		MaxOpenConn: openC,
		Schema:      schema,
	}

}

// create database postgres instance
func InitPostgres() (*gorm.DB, error) {
	log.Default().Println("connecting postgres database")
	config := loadConfig()
	fmt.Println("config postgres:", fmt.Sprintf("%#v", config))

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d ", config.Host, config.Username, config.Password, config.Database, config.Port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Default().Println("connect postgres err:", err)
		return db, err
	}
	// db.DB().SetMaxOpenConns(config.MaxOpenConn)
	// db.DB().SetMaxIdleConns(config.MaxIdleConn)
	log.Default().Println("connect postgres successfully")
	return db, err
}

// mapping error code of postgres database
func MapErrCode(err error) string {
	var per *pgconn.PgError
	if errors.As(err, &per) {
		return mapDuplicateCode(per.Code, per.ConstraintName)
	}
	return common.ErrDatabase.Code()
}

func mapDuplicateCode(code, name string) string {

	switch code {
	case common.CodeDuplicateKey:
		return mapDuplicateName(name)
	}
	return common.ErrDatabase.Code()
}

func mapDuplicateName(name string) string {

	switch name {
	case common.NameDuplicateOrderID:
		return common.ErrOrderIdExists.Code()
	}
	return common.ErrDatabase.Code()

}
