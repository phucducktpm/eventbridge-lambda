package db

import (
	"database/sql"
	"testing"
)

func BenchmarkCashErrorPostgres(b *testing.B) {
	connStr := "postgres://pqgotest:password@localhost/pqgotest?sslmode=verify-full"
	_, err := sql.Open("postgres", connStr)
	for i := 0; i < b.N; i++ {
		MapErrCode(err)
	}
}
