package chttp

import (
	"context"
	"net/http"
	"scheduler-management/internal/dto"
)

// post api
func (h *clientHttp) Get(ctx context.Context,
	req dto.ClientHttpRequest) (httpResp *http.Response, err error) {
	req.Method = http.MethodGet
	reqhttp, err := buildRequestHttp(ctx, req)
	if err != nil {
		return httpResp, err
	}
	httpResp, err = h.client.Do(reqhttp)
	return httpResp, err
}
