package chttp

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"scheduler-management/internal/dto"
	"time"
)

const (
	// contentType
	MimeJSON  = "application/json"
	URLEncode = "application/x-www-form-urlencoded"
	// timeDuration
	TimeoutHttp = 30 * time.Second
)

type ClientHttp interface {
	Post(ctx context.Context, req dto.ClientHttpRequest) (*http.Response, error)
	Get(ctx context.Context, req dto.ClientHttpRequest) (*http.Response, error)
}

type clientHttp struct {
	client *http.Client
}

func timeoutHttp(timeout int) time.Duration {
	if timeout == 0 {
		return time.Duration(TimeoutHttp)
	}
	return time.Duration(timeout) * time.Second
}

func NewClientHttp(timeout int) ClientHttp {
	client := &http.Client{
		Timeout:   timeoutHttp(timeout),
		Transport: getTransport(),
	}

	return &clientHttp{
		client: client,
	}
}

func getTransport() *http.Transport {
	tr := &http.Transport{
		// MaxIdleConns:       10,
		// IdleConnTimeout:    30 * time.Second,
		// DisableCompression: true,
	}
	return tr
}

// build common header
func buildHeader(mapHeader map[string]string) (header http.Header) {
	header = make(http.Header)
	for key, value := range mapHeader {
		header.Set(key, value)
	}
	return header
}

// build body of api
func buildBody(ctx context.Context, contentType string, bodyReq interface{}) (*bytes.Reader, error) {
	var (
		body     *bytes.Reader
		err      error
		bodyByte []byte
	)
	switch contentType {
	default:
		bodyByte, err = json.Marshal(bodyReq)
	}
	if err != nil {
		return body, err
	}
	body = bytes.NewReader(bodyByte)
	return body, err
}

// build request data of http
func buildRequestHttp(ctx context.Context, req dto.ClientHttpRequest) (*http.Request, error) {
	var (
		httpReq *http.Request
		err     error
	)
	body, err := buildBody(ctx, req.ContentType, req.Body)
	if err != nil {
		return httpReq, err
	}
	httpReq, err = http.NewRequestWithContext(ctx, req.Method, req.Url, body)
	if err != nil {
		return httpReq, err
	}
	httpReq.Header = buildHeader(req.Header)
	if req.Query != "" {
		httpReq.URL.RawQuery = req.Query
	}
	return httpReq, err
}

// post api
func (h *clientHttp) Post(ctx context.Context,
	req dto.ClientHttpRequest) (httpResp *http.Response, err error) {
	req.Method = http.MethodPost
	reqhttp, err := buildRequestHttp(ctx, req)
	if err != nil {
		return httpResp, err
	}
	httpResp, err = h.client.Do(reqhttp)
	return httpResp, err
}
