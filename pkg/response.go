package pkg

import (
	"bytes"
	"encoding/json"
	"scheduler-management/internal/common"
	"scheduler-management/pkg/utils"

	"log"
)

type HttpResponse struct {
	ChannelID string
	Uuid      string
	Err       error
	Time      string
	Data      interface{}
}

func bodyResponse(bodyInput HttpResponse) []byte {
	mapResp := make(map[string]interface{})
	mapResp["responseId"] = bodyInput.Uuid
	mapResp["responseTime"] = bodyInput.Time
	switch bodyInput.ChannelID {
	case string(common.ChannelC2c):
		mapResp["responseCode"] = common.ParseError(bodyInput.Err).Code()
		mapResp["responseMessage"] = common.ParseError(bodyInput.Err).MessageC2c()
	default:
		mapResp["responseCode"] = common.ParseError(bodyInput.Err).Code()
		mapResp["responseMessage"] = common.ParseError(bodyInput.Err).Message()
	}

	body, errMarshal := json.Marshal(mapResp)
	if errMarshal != nil {
		log.Default().Println("marshal response err", errMarshal)
	}
	return body
}

// build response data, cast error code, message resposne
func Response(respBody HttpResponse) string {
	respBody.Time = utils.GetTimeNowFormat("2006-01-02T15:04:05.000-07:00")
	var buf bytes.Buffer
	if respBody.Err != nil {
		body := bodyResponse(HttpResponse{
			ChannelID: respBody.ChannelID,
			Uuid:      respBody.Uuid,
			Err:       respBody.Err,
			Time:      respBody.Time,
		})
		json.HTMLEscape(&buf, body)
		return buf.String()
	}
	mapRes := map[string]interface{}{
		"responseId":      respBody.Uuid,
		"responseCode":    "00",
		"responseMessage": "successfully",
		"responseTime":    respBody.Time,
	}
	if respBody.Data != nil {
		mapRes["data"] = respBody.Data
	}
	body, errMarshal := json.Marshal(mapRes)
	if errMarshal != nil {
		log.Default().Println("marshal response err", errMarshal)
	}
	json.HTMLEscape(&buf, body)
	return buf.String()
}
