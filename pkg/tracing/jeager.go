package tracing

import (
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"

	"github.com/uber/jaeger-client-go"
	jaegerConfig "github.com/uber/jaeger-client-go/config"
)

// Init functionr return an instance of jager tracer
func Init(service string) (tracer opentracing.Tracer, closer io.Closer, err error) {
	os.Setenv("JAEGER_SERVICE_NAME", service)
	cfg, err := jaegerConfig.FromEnv()
	if err != nil {
		panic(fmt.Sprintf("ERROR: failed to read config from env vars: %v\n", err))
	}
	tracer, closer, err = cfg.NewTracer(
		jaegerConfig.Logger(jaeger.StdLogger),
	)
	if err != nil {
		return tracer, closer, err
	}
	return tracer, closer, nil
}
func Inject(span opentracing.Span, request *http.Request) error {
	return span.Tracer().Inject(
		span.Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(request.Header))
}

func Extract(tracer opentracing.Tracer, r *http.Request) (opentracing.SpanContext, error) {
	return tracer.Extract(
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(r.Header))
}

func StartSpanFromRequest(tracer opentracing.Tracer, r *http.Request, funcName string) opentracing.Span {
	if r == nil {
		return tracer.StartSpan(funcName, nil)
	}
	spanCtx, _ := Extract(tracer, r)
	return tracer.StartSpan(funcName, ext.RPCServerOption(spanCtx))
}
