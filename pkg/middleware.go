package pkg

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

const (
	hiddenContent         = "<HIDDEN>"
	ignoreContent         = "<IGNORE>"
	emptyContentTag       = "<EMPTY>"
	contentSizeLimitation = 10000
)

func GetRequestBody(ctx *gin.Context) string {
	requestBody := hiddenContent

	buf, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
		zap.S().With("err- ", err).Error("can't read body content from request")

	}
	readCloser1 := ioutil.NopCloser(bytes.NewBuffer(buf))
	// We have to create a new Buffer and transfer it to request body again, because readCloser1 will be read.
	readCloser2 := ioutil.NopCloser(bytes.NewBuffer(buf))
	ctx.Request.Body = readCloser2

	// Convert readCloser1 to String
	bytesBuffer := new(bytes.Buffer)
	_, err = bytesBuffer.ReadFrom(readCloser1)
	if err != nil {
		zap.S().With("err- ", err).Error("can't read byte array from reader")
		return ignoreContent
	}
	requestBody = bytesBuffer.String()
	if requestBody == "" {
		// Return Tag to easy filter
		return emptyContentTag
	}
	return requestBody
}

type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func IgnorePath(path string) bool {
	skip1 := strings.Contains(path, "/liveness")
	skip2 := strings.Contains(path, "/metrics")
	skip3 := strings.Contains(path, "/readiness")
	if skip1 || skip2 || skip3 {
		return true
	}
	return false
}
func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}
func Payload() gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.URL.Path
		if isShip := IgnorePath(path); isShip {
			return
		}
		requestBody := GetRequestBody(c)
		fmt.Printf("Request-API %s: %s \n", path, []byte(requestBody))

		blw := &bodyLogWriter{
			body:           bytes.NewBufferString(""),
			ResponseWriter: c.Writer,
		}
		c.Writer = blw

		c.Next()
		bodyStr := blw.body.String()
		fmt.Printf("Response-API %s:%s \n", path, bodyStr)
		// add metrics
		metricsBodyCode(bodyStr)
	}
}

type response struct {
	ResponseCode    string      `json:"responseCode"`
	ResponseMessage string      `json:"responseMessage,omitempty"`
	ResponseId      string      `json:"responseId,omitempty"`
	ResponseTime    string      `json:"responseTime,omitempty"`
	Data            interface{} `json:"data,omitempty"`
}

func metricsBodyCode(bodyStr string) {
	data := response{}
	err := json.Unmarshal([]byte(bodyStr), &data)
	if err != nil {
		return
	}
}
