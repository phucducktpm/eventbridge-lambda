package qrcode

import (
	"context"
	"scheduler-management/pkg"
	"testing"
)

func TestGenQR(t *testing.T) {
	ctx := context.Background()

	listQR := []QRCodeRequest{
		{
			AccountNo:   "045704050000032",
			AccountName: "DAO VAN SON",
			AcqId:       pkg.HDBankCode,
			Amount:      11000,
			AddInfo:     "xin chao cac ban",
			Format:      "",
			Size:        0,
		},
		{
			AccountNo:   "045959090000268",
			AccountName: "DINH THI PHUONG DUNG",
			AcqId:       pkg.HDBankCode,
			Amount:      100000,
			AddInfo:     "this is11111 123 456",
			Format:      "",
			Size:        0,
		},

		{
			AccountNo:   "020704000000245",
			AccountName: "KHONG THI NGAN",
			AcqId:       pkg.HDBankCode,
			Amount:      1239128,
			AddInfo:     "highest abc content",
			Format:      "HIGHEST",
			Size:        512,
		},
		{
			AccountNo:   "003704070000280",
			AccountName: "CTY TNHH TMDV PHUONG NGA",
			AcqId:       pkg.HDBankCode,
			Amount:      10123123,
			AddInfo:     "xin chao cac ban",
			Format:      "",
			Size:        0,
		},
		{
			AccountNo:   "045704050000040",
			AccountName: "PHAN THI LUY",
			AcqId:       pkg.HDBankCode,
			Amount:      55400,
			AddInfo:     "so tien le",
			Format:      "",
			Size:        0,
		},
		{
			AccountNo:   "045959090000603",
			AccountName: "DIEP THI TRO",
			AcqId:       pkg.HDBankCode,
			Amount:      1232122, // error
			AddInfo:     "xin chao cac ban",
			Format:      "",
			Size:        0,
		},
		{
			AccountNo:   "045959090000603",
			AccountName: "DIEP THI TRO",
			AcqId:       pkg.HDBankCode,
			Amount:      1232122,
			AddInfo:     "xin chao cac ban",
			Format:      "",
			Size:        0,
		},
		{
			AccountNo:   "26102022131247",
			AccountName: "Nguyen Phuc Duc",
			AcqId:       pkg.HDBankCode,
			Amount:      100000,
			AddInfo:     "this is11111 123 456",
			Format:      "",
			Size:        0,
		},
		{
			AccountNo:   "26102022131247vgi3",
			AccountName: "Nguyen Phuc Duc",
			AcqId:       pkg.HDBankCode,
			Amount:      100000,
			AddInfo:     "this is11111 123 456",
			Format:      "",
			Size:        0,
		},
		{
			AccountNo:   "002704070000186",
			AccountName: "NGUYEN QUOC KHANH",
			AcqId:       pkg.HDBankCode,
			Amount:      123031,
			AddInfo:     "high xin chao cac ban",
			Format:      "HIGH",
			Size:        100,
		},
	}
	for _, item := range listQR {
		_, _, err := GenQrCode(ctx, item)
		if err != nil {
			t.Error(err)
		}

	}
}
