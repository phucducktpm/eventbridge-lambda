package qrcode

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"scheduler-management/pkg"
	"scheduler-management/pkg/utils"
	"strings"

	"github.com/Rhymond/go-money"
	sigurncrc16 "github.com/sigurn/crc16"

	"github.com/skip2/go-qrcode"
)

type QRCodeRequest struct {
	MerchantId  string
	TerminalId  string
	AccountNo   string  `json:"accountNo"`
	AcqId       string  `json:"acqId"` // bankCode
	AccountName string  `json:"accountName"`
	AddInfo     string  `json:"addInfo"` // description
	Format      string  `json:"format"`
	Amount      float64 `json:"amount"`
	Expire      string  `json:"expire"`
	Invoice     string  `json:"invoice"`
	Size        int     `json:"size"`
}

func getFormat(format string) qrcode.RecoveryLevel {
	var level = qrcode.Medium
	format = strings.ToUpper(format)
	switch format {
	case "LOW":
		level = qrcode.Low
	case "HIGH":
		level = qrcode.High
	case "HIGHEST":
		level = qrcode.Highest
	}
	return level
}
func getSize(size int) int {
	if size != 0 {
		return size
	}
	return 256
}

func buildCRCHash(con string) string {
	con = con + "6304"
	table := sigurncrc16.MakeTable(sigurncrc16.CRC16_CCITT_FALSE)
	h := sigurncrc16.New(table)
	h.Write([]byte(con))
	sum := h.Sum16()
	crc := fmt.Sprintf("%X", sum)
	return crc
}

func buildAddInfoTag08(description string) string {
	lenTag := len(description)
	if lenTag < 10 {
		// prefix '0', because ned 2 character
		return fmt.Sprintf("0%d", lenTag) + description
	}
	return fmt.Sprintf("%d", lenTag) + description
}

func buildAddInfoTag03(mid string) string {
	lenTag := len(mid)
	if lenTag < 10 {
		// prefix '0', because ned 2 character
		return fmt.Sprintf("0%d", lenTag) + mid
	}
	return fmt.Sprintf("%d", lenTag) + mid
}

// add tag05: expire, invoice
func buildAddInfoTag05(expire, invoice string) string {
	tag05 := ""
	if expire != "" {
		tag05 = tag05 + fmt.Sprintf("01%s", expire)
	}
	if invoice != "" {
		tag05 = tag05 + invoice
	}
	return fmt.Sprintf("%d", len(tag05)) + tag05
}

func buildAddInfoTag07(tid string) string {
	lenTag := len(tid)

	if lenTag < 10 {
		// prefix '0', because ned 2 character
		return fmt.Sprintf("0%d", lenTag) + tid
	}
	return fmt.Sprintf("%d", lenTag) + tid
}

type (
	QRField62 struct {
		Expire      string
		Tid         string
		Description string
		Mid         string
		Invoice     string
	}
)

func buildAddInfo(req QRField62) (string, error) {
	var (
		info, tag03, tag05, tag07, tag08 string
		lenTag                           int
		isCheckLenDesc                   bool
	)
	fmt.Println(tag05)
	if req.Mid != "" {
		tag03 = buildAddInfoTag03(req.Mid)
		lenTag = lenTag + len(tag03) + pkg.NapasFixAddInfoLen

		strTag03 := fmt.Sprintf("03%s", tag03)
		info = info + strTag03
	}

	if req.Expire != "" || req.Invoice != "" {
		log.Default().Println("ssssssssssssssssss")
		tag05 = buildAddInfoTag05(req.Expire, req.Invoice)
		lenTag = lenTag + len(tag05) + 2
		strTag05 := fmt.Sprintf("05%s", tag05)
		info = info + strTag05
		isCheckLenDesc = true
	}

	if req.Tid != "" {
		tag07 = buildAddInfoTag07(req.Tid)
		lenTag = lenTag + len(tag07) + pkg.NapasFixAddInfoLen
		strTag07 := fmt.Sprintf("07%s", tag07)
		info = info + strTag07
	}
	req.Description = strings.TrimSpace(req.Description)
	if req.Description == "empty-note" {
		req.Description = " "
	}
	// fmt.Println("len-description", len(description))
	if req.Description != "" {
		if isCheckLenDesc && len(req.Description) > 10 {
			return info, fmt.Errorf("description over length")
		}
		fmt.Println("len-description gen tag 08", len(req.Description))
		tag08 = buildAddInfoTag08(req.Description)
		lenTag = lenTag + len(tag08) + pkg.NapasFixAddInfoLen
		info = info + fmt.Sprintf("08%s", tag08)
	}

	info = fmt.Sprintf("62%s%s", fmt.Sprintf("%d", lenTag), info)
	return info, nil
}

// func buildAddInfo(description string) string {
// 	var info string
// 	tag08 := buildAddInfoTag08(description)
// 	lenInfo := len(tag08) + pkg.NapasFixAddInfoLen
// 	info = fmt.Sprintf(pkg.NapasAddInfo, fmt.Sprintf("%d", lenInfo), tag08)
// 	return info
// }

func buildCustomerInfoTag00() string {
	tag00 := "0010A000000727"
	return tag00
}

func buildCustomerInfoTag01Tag00(accNo string) string {
	tag00 := "0006970437"
	return tag00
}
func buildCustomerInfoTag01Tag01(accNo string) string {
	tag01 := fmt.Sprintf("01%s%s", fmt.Sprintf("%d", len(accNo)), accNo)
	return tag01
}
func buildCustomerInfoTag01(accNo string) string {
	str := buildCustomerInfoTag01Tag00(accNo) + buildCustomerInfoTag01Tag01(accNo)
	tag01 := fmt.Sprintf("01%s%s", fmt.Sprintf("%d", len(str)), str)
	return tag01
}

func buildCustomerInfoTag02() string {
	tag02 := "0208QRIBFTTA"
	return tag02
}

func buildCustomerInformation(accNo string) string {
	out := buildCustomerInfoTag00() + buildCustomerInfoTag01(accNo) + buildCustomerInfoTag02()
	return fmt.Sprintf(pkg.NapasCusomerInformation, fmt.Sprintf("%d", len(out)), out)
}

func BuildAmount(amount float64) string {
	var (
		amountContent string
		format        string = "540%d%s"
	)
	amountStr := money.New(int64(amount), money.VND).Display()
	amountStr = strings.Replace(amountStr, "₫", "VND", -1)
	lenAmount := len(amountStr)
	if lenAmount >= 10 {
		format = "54%d%s"
	}
	amountContent = fmt.Sprintf(format, lenAmount, amountStr)
	return amountContent
}

func buildContent(ctx context.Context, req QRCodeRequest) (string, error) {

	// fmt.Println("dataBuildContent:", fmt.Sprintf("%#v", req))

	var (
		consumerInfo = ""
		addInfo      = ""
		amountStr    = ""
	)
	content := pkg.NapasPayLoadFormat + pkg.NapasInitMethod
	if req.Amount != 0 {
		lenAmount := len(utils.FormatFloatToString(req.Amount))
		var (
			format string = "540%d%s"
		)
		if lenAmount >= 10 {
			format = "54%d%s"
		}
		amountStr = fmt.Sprintf(format, lenAmount, utils.FormatFloatToString(req.Amount))
	}
	content = content + amountStr
	// consumer acount information
	if req.AccountNo != "" {
		consumerInfo = buildCustomerInformation(req.AccountNo)
	}
	content = content + consumerInfo + pkg.NapasCurrencyVND + pkg.NapasCountryCodeVND
	// addInfo = buildAddInfo(req.AddInfo, req.MerchantId, req.TerminalId)
	addInfo, err := buildAddInfo(QRField62{
		Expire:      req.Expire,
		Tid:         req.TerminalId,
		Description: req.AddInfo,
		Mid:         req.MerchantId,
		Invoice:     req.Invoice,
	})
	if err != nil {
		return content, err
	}
	content = content + addInfo

	// content = "000201010212262200069704370108621QDTTG5204822053037045405500005802VN5924DAI HOC NGUYEN TAT THANH6003HCM62880317DH NGUYENTATTHANH053101211105112646a6b5a2411112111140705DHNTT0819012807000601_Ky ..."
	// content = "000201010212262200069704370108621QDTTG5204822053037045405500005802VN5924DAI HOC NGUYEN TAT THANH6003HCM62880317DH NGUYENTATTHANH0531012111051126QR240220230859151UD0705DHNTT0819012807000601_Ky ..."

	var hashCRC string
	for i := 0; i <= 3; i++ {
		hashCRC = buildCRCHash(content)
		if len(hashCRC) == 4 {
			break
		}
	}

	if len(hashCRC) != 4 {
		log.Default().Println("hashCRC break ", hashCRC)
		return "HASHCRC-ERROR", nil
	}
	crc := fmt.Sprintf(pkg.NapasCRC, hashCRC)

	content = content + crc
	return content, nil
}

func GenQrCode(ctx context.Context, req QRCodeRequest) (string, string, error) {
	var (
		qrBase64 string
		png      []byte
		content  string
		err      error
	)
	for i := 0; i <= 3; i++ {
		content, err = buildContent(ctx, req)
		if err != nil {
			return qrBase64, content, err
		}
		if content == "HASHCRC-ERROR" {
			req.AddInfo = "empty-note"
			continue
		} else {
			break
		}
	}

	log.Default().Printf("%s: content-qr: %s \n", req.AccountNo, content)
	png, err = qrcode.Encode(content, getFormat(req.Format), getSize(req.Size))
	if err != nil {
		return qrBase64, content, fmt.Errorf("gen qr-code err: %s", err)
	}
	qrBase64 = base64.StdEncoding.EncodeToString(png)
	fmt.Println(qrBase64)

	return qrBase64, content, err
}
