package utils

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"strings"
	"time"

	"github.com/google/uuid"
)

var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// letter-only upper
var upperLetters = []rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")

// random letter with lenght = n, inclue character in letters array
func randString(n int, letters []rune) string {
	rand.Seed(GetTimeNow().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func nowAsUnixSecond() int64 {
	return GetTimeNow().UnixNano() / 1e9
}

// GetID represents get unique id for transaction
func GetID() string {
	dest, _ := hex.DecodeString(fmt.Sprintf("%d", nowAsUnixSecond()))
	var id strings.Builder
	encode := base64.StdEncoding.EncodeToString(dest)
	rand.Seed(GetTimeNow().UnixNano())
	id.WriteString(encode)
	id.WriteString(randString(4, letters))
	return strings.Replace(id.String(), "=", randString(1, letters), 1) + randString(4, letters)
}

// gen id with format DDMMYYYYhh24miss{4}[a-zA-Z]
func GetIDTime() string {
	timeNow := GetTimeNow()
	id := timeNow.Format("02012006150405") + randString(3, upperLetters)
	return id
}

func GetTranID() string {
	timeNow := GetTimeNow()
	id := timeNow.Format("020106") + randString(5, upperLetters)
	return id
}

// gen-trace-id
var lettersOnlyNumber = []rune("0123456789")

func GetTraceID(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = lettersOnlyNumber[rand.Intn(len(lettersOnlyNumber))]
	}
	return string(b)
}

// generate RequestID
func RequestID(reqID string, contextID string) string {
	if reqID != "" {
		return reqID
	}
	var id string = contextID
	if reqID != "" {
		id = fmt.Sprintf("%s:%s", reqID, contextID)
	}
	if id != "" {
		return id
	}
	log.Default().Println("request-id is a empty, system automation gen request-id equal uuid")
	return uuid.NewString()
}
