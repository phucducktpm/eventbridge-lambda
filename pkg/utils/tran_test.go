package utils

import (
	"fmt"
	"testing"
)

func TestTransactionId(t *testing.T) {
	var mapIds = make(map[string]int)
	now := GetTimeNow()
	for i := 0; i < 10000; i++ {
		id := GetIDTime()
		fmt.Println("id gen:", id)
		va, is := mapIds[id]
		if is {
			t.Errorf("tranactionId exists %d, position %s", va, id)
		}
		mapIds[id] = i
	}
	TraceTotalTime(now, " test-gen-transactionID:")

}
