package utils

import (
	"fmt"
	"reflect"
	"regexp"
	"runtime"
	"strconv"
	"strings"
)

func GetSlashOs() string {
	if strings.Contains(runtime.GOOS, "windows") {
		return "\\"
	}
	return "/"
}
func FindInArray(arr []string, value string) bool {
	for _, item := range arr {
		if strings.EqualFold(item, value) {
			return true
		}
	}
	return false
}

func ValidatorUUID(uuid string) error {
	pile := "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
	reg := regexp.MustCompile(pile)
	isValid := reg.MatchString(uuid)
	if !isValid {
		return fmt.Errorf("invalid uuid")
	}
	return nil
}

func FormatFloatToString(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}

// trim-space all field in struct
func TrimSpace(obj interface{}) interface{} {
	var objOut interface{}

	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
			objOut = obj
		}
	}()

	// values := reflect.ValueOf(obj)
	// types := values.Type()
	// for i := 0; i < values.NumField(); i++ {
	// 	fmt.Println(types.Field(i).Index[0], types.Field(i).Name, values.Field(i))
	// }

	msValuePtr := reflect.ValueOf(obj)
	fmt.Println(reflect.TypeOf(obj).Kind())
	if reflect.TypeOf(obj).Kind() == reflect.Pointer {
		msValuePtr = msValuePtr.Elem()
	}
	fmt.Println(reflect.TypeOf(obj).Kind())

	for i := 0; i < msValuePtr.NumField(); i++ {
		field := msValuePtr.Field(i)

		fieldValue := reflect.ValueOf(msValuePtr.Field(i))
		fmt.Println(fieldValue)
		fmt.Println(field.Type())

		// Ignore fields that don't have the same type as a string
		if field.Type() != reflect.TypeOf("") {
			continue
		}

		str := field.Interface().(string)
		str = strings.TrimSpace(str)
		fmt.Println(reflect.TypeOf(field.Interface()).Kind())
		if field.CanSet() {
			field.SetString(str)
		}
	}
	fmt.Println(objOut)
	return objOut
}
