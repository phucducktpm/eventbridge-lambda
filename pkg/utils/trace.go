package utils

import (
	"fmt"
	"time"
)

// tracing total handle of function, return millisecond
func TraceTotalTime(start time.Time, message string) {
	milli := time.Since(start).Milliseconds()
	if milli > 200 {
		fmt.Printf("WARNING: total time handle %s, %d \n", message, milli)
	} else {
		fmt.Printf("total time handle %s, %d \n", message, milli)
	}
}
