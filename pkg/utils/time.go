package utils

import (
	"log"
	"time"
)

func TimeInitLoc(locStr string) *time.Location {
	defer func() {
		errR := recover()
		if errR != nil {
			log.Default().Println("ERROR-ERROR:", errR)
		}
	}()
	var loc *time.Location
	var err error
	if locStr == "" {
		loc, err = time.LoadLocation("Asia/Ho_Chi_Minh")
	} else {
		// TODO parse location
		loc, err = time.LoadLocation(locStr)
	}
	if err != nil {
		log.Default().Println("ERROR-ERROR:", err)
	}
	return loc
}
func TimeInit(locStr string) time.Time {
	defer func() {
		errR := recover()
		if errR != nil {
			log.Default().Println("ERROR-ERROR:", errR)
		}
	}()
	loc := TimeInitLoc("")
	if loc == nil {
		return time.Now()
	}
	now := time.Now().In(loc)
	return now
}

func GetTimeNow() time.Time {
	return TimeInit("")
}

func GetTimeNowFormat(format string) string {
	now := GetTimeNow()
	if format == "" {
		format = time.RFC3339
	}
	return now.Format(format)
}

func TimeParse(format, value string) (time.Time, error) {
	TimeInit("")
	if format == "" {
		format = time.RFC3339
	}
	return time.Parse(format, value)
}

func GetTimeFormatDefault() string {
	return time.RFC3339
}

func ValidatorTime(value string) error {
	_, err := time.Parse(GetTimeFormatDefault(), value)
	return err
}
