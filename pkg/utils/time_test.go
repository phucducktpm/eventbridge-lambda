package utils

import (
	"testing"
)

type StructTestValidatorTime struct {
	Input    string
	Expected bool
}

func TestValidatorTime(t *testing.T) {
	input := []StructTestValidatorTime{
		{
			Input:    "2022-10-31T09:12:44+07:00",
			Expected: true,
		},
		{
			Input:    "Mon, 31 Oct 2022 09:13:07 +07",
			Expected: false,
		},
	}
	for _, item := range input {
		is := true
		err := ValidatorTime(item.Input)
		if err != nil {
			is = false
		}
		if is != item.Expected {
			t.Error("test fail")

		}
	}
}
