package pkg

// aws
const (
	RegionDefault       = ""
	BucketDefault       = ""
	CollectionIDDefault = ""
	TableNameDefault    = ""
)

const HDBankCode string = "970437"

// Viet-qrcode
const (
	VietQRFormat         string = "text"
	VietQRTemplteCompact string = "compact"
	VietQRTemplteQROnly  string = "qr_only"
	VietQRURL            string = "https://api.vietqr.io/v2/generate"
)

// signature-func
const (
	SignatureGenQR    string = "GENQR"
	SignatureSearchQR string = "SEARCHQR"
	SignatureUpdateQR string = "UPDATEQR"
)

// napas
var (
	NapasCurrencyVND    = "5303704"
	NapasCountryCodeVND = "5802VN"
	NapasInitMethod     = "010212" // or 010212
	NapasPayLoadFormat  = "000201"
	NapasAddInfo        = "62%s08%s"
	NapasFixAddInfoLen  = 2
	// NapasCusomerInformation = "38%s0010A00000072701%s00069704370113%s0208QRIBFTTA"
	NapasCusomerInformation = "38%s%s"
	// NapasCusomerInformationTag01    = "01%s00069704370113%s"
	// NapasCusomerInformationTag01Len = 14

	NapasFixCusomerInformationLen = 44
	NapasCRC                      = "6304%s"
	NapasAmount                   = "540%d%s"
)
