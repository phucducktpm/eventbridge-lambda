#!/bin/bash
echo "Store timestamp deploy..."
CURRENTDATE=`date +"%Y-%m-%d %T"`
touch ./log.txt
echo "\n-----------" ${CURRENTDATE} "-----------" >> log.txt
sls deploy list >> log.txt
echo "Latest runtime:"
tac log.txt | grep Timestamp | head -n 1
